FROM node:10-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY ./src/package*.json ./

USER node
ENV PATH=$PATH:/home/node/app/node_modules/.bin

RUN npm ci

COPY --chown=node:node ./src .

RUN ls -la
COPY ./src/public ./dist/public

EXPOSE 3000

CMD [ "node", "./bin/www" ]
