// Import Axios HTTP requests Library
const axios = require('axios');
const debug = require('debug')('tagshelfinterview:languageLayerService');

// Language Layer API endpoint URL
const HTTP_DETECTION_ENDPOINT = 'http://api.languagelayer.com/detect';
// Language Layer API Key
const HTTP_DETECTION_ENDPOINT_API_KEY = '27851b9da25abf91b8aa97de0c6e94c3';

// Exports a 'detect' function that can be accessed when require'd
module.exports.detect = async (text) => {
    try{
        //Debug Message to see data in transit
        debug("Attempting to detect: " + text);
        // Waits for the Axios promise to fulfill.
        // Axios automatically URL-encodes our data.
        const ans = await axios.get(HTTP_DETECTION_ENDPOINT,{
            params: {
                access_key: HTTP_DETECTION_ENDPOINT_API_KEY,
                query: text
            }
        });

        // Returns only the relevant part of the Response object.
        debug("Detect successful: " + ans.data.success);

        if (!ans.data.success){
            // If we were not successful, return empty.
            return {
                reliable: [],
                results: []
            };
        }
        let data = ans.data.results;

        data.forEach(lang => {
            // Calculate a score based on the measurements provided by the API
            lang.score = (lang.percentage + ((lang.probability/100.00) * lang.percentage));

            // Promote reliable results
            if(lang.reliable_result){
                lang.score += lang.score * 0.25;
            }
        });

        //Sorts descending by confidence and returns top 3 (most confident)
        data = data.sort((a, b)=> b.score - a.score);
        data = data.slice(0, 3);
        debug("Results after first filter: ");
        debug(data);

        // If there is a reliable result, select it. If not, null.
        let reliable = data.filter(lang => lang.reliable_result);

        return {
            reliable: reliable,
            results: data
        };
    }
    catch (e) {
        // Catch all error handler. Returns {'success:' "false"}
        debug("Got error: " + e);
        return {
            success: false,
        }
    }
};
