const express = require('express');
const router = express.Router();
const {detect} = require('../services/languageLayer');
var debug = require('debug')('tagshelfinterview:apiRoute');

router.get('/parse', async function(req, res, next) {
  if(!req.query.q){
    debug("No parameter q detected in URL");
    res.status(400);
  }
  // Calls imported method from LanguageLayer Service and waits for async response
  const ans = await detect(req.query.q);

  // If we got a reliable result from the server, return it.
  if(ans.reliable.length){
    res.send({
      success: true,
      exact: true,
      language: ans.reliable[0].language_name,
    });
  }

  // If no reliable results, return the first language based on calculated score.
  else if(ans.results.length){
    res.send({
      success: true,
      exact: false,
      language: ans.results[0].language_name,
    });
  }

  // An unknown error occurred.
  else res.send({
      success: false,
      exact: false,
      language: "",
    });
});

module.exports = router;
